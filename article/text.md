<style>
html {
  font-size: 14pt;
}
body {
  width: 40rem;
  margin: auto;
  padding: 1rem;
}
img {
  max-width: 100%;
}
figure {
  padding: 1rem;
  margin: 2rem;
  background-color:#F8F8F8;
  border:solid 1px #EEE;
  text-align: center;
}
figcaption {
  font-size: 12pt;
  margin-top: 0.5rem;
  font-family: sans-serif;
}
hr {
  border: none;
  outline: none;
  height: 1px;
  background-color: #DDD;
}
blockquote {
  font-style: italic;
}
</style>

`Jgs and ASCII art, Adel Faure, 2022`

What is ASCII art ?
-------------------

![Starry Night by Veni, Vidi, ASCII](img/starry_night_by_venividiascii.png)

Explaining exactly what is ASCII art is in my sense a difficult task. The
pratice on itself kind of blurrying a common opposition in art between image
and text.

ASCII art originally meant pictures made using characters present in the
American Standard Code for Information Intercahnge (abbreviated ASCII). While
"Text art" or "Textmode art" terms are also used, "ASCII art" eventually became a
metonymy for denoting any computer artwork created using text.

As Joan G. Stark pointed it out

> They are "non-graphical graphics". Its palette is limited to the symbols
> and characters that you have available to you on your computer keyboard.[^1]

Starting from this principle we can understand that each keyboard-computer
association got its own "ASCII art".

Thats how we find PETSCII associated with Commodore, ANSI with BBS (Bulletin
Board Systems), ATASCII whith Atari, Shift-JIS with japanese keyboards,
Teletext for videotext (Prestel, Minitel)… ASCII art designating specifically
both Amiga ASCII style (oldschool and newschool) and Usenet ASCII (line-style
and solid-style).[^2]

Each ASCII art categories and subcategories having its own scenes, groups, artists and
sometimes even its own dedicated platform :

- [www.asciiarena.se](https://www.asciiarena.se) for Amiga style ASCII
- [www.16colo.rs](https://www.16colo.rs) for ANSI art
- [www.csdb.dk](https://www.csdb.dk) for PETSCII
- [www.teletextart.co.uk](https://www.teletextart.co.uk) for Teletext art

When at the end of the XXE century computing became widespread, ASCII art was a
way to produce figurative content on systems that didn't or hardly support
images.

On the emerging ANSI art scene, it a way for users with lesser technical skills
to gained access to BBSs contents in returns for producing art pieces.[^3]

On the first forums networks it was used to produce memes before the
possbilities to share pictures. On
[www.asciiartfarts.com](http://www.asciiartfarts.com/) numerous examples (for
better or worse) of theses text-based memes can be found. Moreover, the design
of some popular memes characters came from ASCII art. It is the case for "Kuma"
(later Pedobear) or "Domo" that came from SHIFT-JIS art pieces shared on 2chan.

[To the gallery](#gallery)

Joan G. Stark
-------------

![Joan G. Stark autoportrait and standard signature](img/jgs.png)


When I started to take interest in ASCII art I quickly came across Joan G Stark
work, feeling that I was looking at something I already knew. Whithout being
sure, it felt like looking again at drawings I saw during my childhood,
recalling the first times I experienced the internet. What is certain is that
Stark was an impressively prolific artist during the 90s and 2000s, leaving
behind a strong legacy on internet aesthetics and vernacular practices. Her
style is straight-forward, like a text mode _clear line_. She most likely drew
every possible animal, plant and monter. For each gesture and scene of everyday
life you can find one of her drawings. The same thing can be said for each
element that constitutes a landscape. 

Malheuresement Jgs à arrêter de publier depuis le début des années 2000. Son
succès et cette diversité dans son travail l'ayant exposé à de très nombreux
détournement de ses oeuvres sans sa permission ni attribution ou compensation,
ce que Joan à finit par ne plus supporter. As ldb sums it in " I Like Making
ASCII Art " :

> Joan was the most prolific and later, the most broken hearted as more and
> more of her ASCII art was stolen – credit for the work ripped off or claimed
> by someone else. [^4]

While today I didn't know personnaly Joan G. Stark but this name is still
resonnating in ascii art "themed" channels on discord, reddit, social medias
RESPECT ASCII ARTIST CAMPAIGN
https://asciiartist.com/from-the-original-respect-ascii-artists-campaign-page/

Jgs Font design
---------------

supprimer, effacer la discontinuité entre les charactères, faire oublier que c'est des caractères

En faisant cela je propose une sorte de style d'ascii particulier.


Concerning specifically Jgs Font approach to ASCII art. Though it includes
CP437 characters set (ANSI art) it was designed while studying Usenet ASCII art
line-style (or oldschool), a style specific to the Usenet ASCII artists at work
between the 80s and 2000s among which Joan G Stark figures as emblematic.

While being a very strong scene at the time, leaving an immense impact on
internet aesthetic, there are few Usenet artists still active today and many
sites and links relative to the scene are now dead.

Trying to give you an idea of what this scene production look like, I've
gathered here the links and text files that I use as references to work on Jgs
Font :

ASCII art tutorials :

-> Daniel C. Au (dcau)
-> Jonathon R. Oglesbee (JRO)
-> Susie Oviatt
-> Normand Veilleux
-> Rowan Crawford (Row)
-> Maija Haavisto (DiamonDie, mh)
-> Targon
-> Joan G Stark

ASCII art Faqs

-> Bob Allison (Scarecrow)
-> Jorn Barger

Gallery
-------

A totally arbitrary and non-exhaustive gallery showing different styles/techniques of ASCII-art.

![The original "Kuma" of 2chan (found on [www.knowyourmeme.com](https://knowyourmeme.com/memes/pedobear))](img/kuma.gif)

![Compilation of 2chan characters (found on [www.outsiderjapan.pbworks.com)](http://outsiderjapan.pbworks.com/w/page/9758331/2channel?mode=print)](img/2ch_AA_Characters.gif)

![Cage, dwimmer, 2021 - PETSCII](img/DWIMMER-CAGE.PNG)

![Snow, LDA, 2021 - ANSI](img/LDA-SNOW.ANS.png)

![Croweye, Specter, 2021 - ATASCII](img/croweye_by_specter.png)

![妖怪がしゃどくろ, 機動戦艦艦長, 2022 - SHIFT-JIS](img/rich_beatle.jpg)

![The Giant's Causeway, SimplySarah, 2021 - TELETEXT](img/simply_sarah.png)

![From "Alice in Wonderland" collection, Allen Mullen, 1999? (exact date unknown) - Newschool ASCII](img/allen_mullen.png)

![Dragon, Joan G Stark, 1996? (exact date unknown) - Oldschool ASCII](img/jgs_dragon.png)


[^1]: [The History of ASCII (Text) Art
by Joan G. Stark](https://www.roysac.com/asciiarthistory.html)
[^2]: Aesthetic features of PETSCII, ANSI and ASCII [What is Textmode by Polyducks](http://polyducks.co.uk/what-is-textmode/)
[^3]: The history of the changing status of ASCII art [ASCII art From a Commodity Into an Obscurity by Heikki Lotvonen](./docs/ASCII_art_From_a_Commodity_Into_an_Obscurity.pdf)
[^4]: Being an ascii artist since 1996 [I Like Making ASCII Art](https://asciiartist.com/i-like-making-ascii-art/)

ASCII ART TUTS

https://www.ludd.ltu.se/~vk/pics/ascii/junkyard/techstuff/tutorials/Hayley_Wakenshaw.html
