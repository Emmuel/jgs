<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr-FR" xml:lang="fr-FR">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Adel Faure" />
  <meta name="dcterms.date" content="2023-01-01" />
  <title>À propos de l’ASCII Art et de Jgs Font</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    div.columns{display: flex; gap: min(4vw, 1.5em);}
    div.column{flex: auto; overflow-x: auto;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
    ul.task-list li input[type="checkbox"] {
      width: 0.8em;
      margin: 0 0.8em 0.2em -1.6em;
      vertical-align: middle;
    }
    .display.math{display: block; text-align: center; margin: 0.5rem auto;}
  </style>
  <link rel="stylesheet" href="style.css" />
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<header id="title-block-header">
<h1 class="title">À propos de l’ASCII Art et de Jgs Font</h1>
<p class="author">Adel Faure</p>
<p class="date">2023</p>
</header>
<!-- Espaces fines -->
<h2 id="introduction">Introduction</h2>
<p>Je suis Adel Faure, ASCII artiste au sein du groupe Mistigris et
Textmode Friends. J’ai été généreusment invité par Velvetyne à publier
Jgs Font via leur fonderie. Jgs Font une police de caractère que j’ai
créer en hommage à l’artiste Joan G. Stark et que je j’utilise pour
faire de l’ASCII art (voir le specimen).</p>
<p>Dans cet article je contextualise ce qu’est l’ASCII art, qui est Joan
G. Stark, ce que pourrait être «une histoire des arts en mode texte»,
qu’est ce que signifie l’ASCII art de nos jours, et quelles sont les
spécificités de la Jgs Font.</p>
<p>Je tiens à remercier chaleureusement Heikki Lotvonen pour m’avoir
partager deux références iconographiques (printersgrammarw0000smit_0153,
Improvisation, Alfred P. Fluhr, fin XVIII) ainsi que pour son texte
<em>ASCII art : From a Commodity Into an Obscurity</em> qui m’a beaucoup
aidé.</p>
<p>Je remercie également Raphaël Bastide, Ève Gauthier et Vincent
Maillard pour m’avoir relu et aidé à terminer ce texte.</p>
<p>Bonne lecture !</p>
<h2 id="lascii-art-cest-quoi">L’ASCII Art c’est quoi ?</h2>
<figure>
<img src="img/starry_night_by_venividiascii.png"
alt="Starry Night, Veni, Vidi, ASCII, 2020" />
<figcaption aria-hidden="true">Starry Night, Veni, Vidi, ASCII,
2020</figcaption>
</figure>
<p>Expliquer ce que signifie ASCII Art n’est pas simple. Plutôt que de
délimiter une pratique bien définie, l’ASCII Art vient brouiller une
distinction commune entre texte et image, dans le domaine artistique, et
entre «interface graphique» et «mode texte», en informatique.</p>
<p>Au sens strict, l’expression designe les images composées à l’aide
des 128 caractères présents dans l’<em>American Standard Code for
Information Interchange</em> (abbrévié ASCII). Bien que «Text Art» ou
«Textmode Art» soit aussi employé, «ASCII Art» ou plus simplement
«ASCII» est devenu une manière de décrire toute image produite à l’aide
d’éléments typographiques. En 1999 dans <em>The History of ASCII (text)
Art</em>, Joan G. Stark décrit l’ASCII de la manière suivante :</p>
<blockquote>
<p>They are “non-graphical graphics”. Its palette is limited to the
symbols and characters that you have available to you on your computer
keyboard.<a href="#fn1" class="footnote-ref" id="fnref1"
role="doc-noteref"><sup>1</sup></a></p>
</blockquote>
<blockquote>
<p>Ce sont des «élements graphiques non-graphiques». Leur palette se
limite aux symboles et caractères que ton clavier d’ordinateur te permet
d’utiliser.</p>
</blockquote>
<figure>
<img src="img/jgs.png"
alt="Un autoportrait de Joan G. Stark accompagné de sa signature standard" />
<figcaption aria-hidden="true">Un autoportrait de Joan G. Stark
accompagné de sa signature standard</figcaption>
</figure>
<p>Joan G. Stark A.K.A jgs ou Spunk est probablement l’ASCII artiste la
plus populaire et prolifique des années 90 et 2000, laissant une très
forte empreinte sur les pratiques et les esthétiques amatrices
d’internet. Stark commence à faire de l’ASCII art en 1995 au sein du
newsgroup &lt;alt.ascii-art&gt; de USENET. Passionnée de folklore et
d’art populaire elle s’attache à représenter dans un style «line-style»
(que l’on pourrait rapprocher de ce que la ligne claire est à la bande
dessinée) d’innombrables créatures mythologiques, animaux, éléments de
paysage, objets et scènes du quotidien. Elle publie l’ensemble de ses
dessins ainsi que des textes sur l’ASCII, sa pratique et son histoire,
sur son site www.ascii-art.com. Bien qu’il soit aujourd’hui hors-ligne,
on le trouve archivé sur de nombreuses adresses <a
href="https://web.archive.org/web/20091028013825/http://www.geocities.com/SoHo/7373/">comme
celle-ci</a>.</p>
<p>Sa définition de l’ASCII comme des «élements graphiques
non-graphiques» joue sur l’ambiguïté de sens, entre le terme anglais
«graphic», comme objet figuratif et «graphic», comme élément d’une
interface graphique. À l’époque où Stark écrit, les premiers réseaux
sociaux (Usenet, BBS, Minitel, Ceefax, etc.), encore très populaires,
fonctionnent «en mode texte». Des interfaces dans lesquelles l’écran est
comme un quadrillage où chaque case permet d’afficher un glyphe. Tandis
que ces dernières disparaissent progressivement au profit des interfaces
graphiques, Stark souligne avec ironie cette ambiguïté du statut de
l’ASCII Art. La présence d’éléments graphiques dans des environnements
textuels devenant cette bizarerie ASCII.</p>
<p>Tout en assumant cette complexité, Stark ramène la pratique de
l’ASCII à une chose très simple : il s’agit de dessiner avec ce que nous
propose un clavier d’ordinateur. «Leur palette se limite aux symboles et
caractères que ton clavier d’ordinateur te permet d’utiliser». Partant
de ce principe il faut imaginer que chaque système associé à un clavier
mène à un ASCII différent. C’est ainsi que nous trouvons le PETSCII
associé au Commodore PET/CBM, l’ANSI avec les BBS (Bulletin Board
Systems), l’ATASCII avec Atari, le Shift-JIS avec le mode Katakana des
claviers japonais, le Teletext pour le Videotext (Prestel, Minitel).
Dans cette galaxie, l’expression ASCII désigne plus spécifiquement le
style Amiga (oldschool et newschool) ou le style Usenet (line-style et
solid-style). Chacun de ces ASCII ayant sa propre scène, avec ses
groupes, ses artistes et parfois même sa propre plateforme de
publication.</p>
<ul>
<li><a href="https://www.asciiarena.se">www.asciiarena.se</a> - Amiga
ASCII</li>
<li><a href="https://www.16colo.rs">www.16colo.rs</a> - ANSI</li>
<li><a href="https://www.csdb.dk">www.csdb.dk</a> - PETSCII</li>
<li><a href="https://www.teletextart.co.uk">www.teletextart.co.uk</a> -
Teletext</li>
</ul>
<h2 id="quelques-exemples">Quelques exemples</h2>
<p>De la même manière que chaque système peut posséder son ASCII art
spécifique. Chaque style à son origine, sa pratique et son histoire
propre.</p>
<p>Le jeu de caractères PETSCII, dessiné en grande partie par Chuck
Peddle, designer du Commodore PET et par Leonard Tramiel, fils du
directeur de Commodore, inclu des trames et des formes géométriques, ce
qui facilite la création de jeux sur un système strictement limité à un
affichage en mode texte.</p>
<figure>
<img src="img/joust-commodore-pet-cbm-screenshot-unhorsed.png"
alt="Joust, The Code Works, 1980, Commodore PET/CBM, frame d’un jeu composé de caractères PETSCII" />
<figcaption aria-hidden="true">Joust, The Code Works, 1980, Commodore
PET/CBM, frame d’un jeu composé de caractères PETSCII</figcaption>
</figure>
<p>Dans <em>ASCII art : From a Commodity Into an Obscurity</em>, Heikki
Lotvonen, rapelle le rôle social de l’ASCII art dans la scène ANSI
émergeante. Les utilisateurs non-hackers mais doués en ASCII art
pouvaient obtenir l’accès à des contenus de BBS pirates en échange
d’illustrations<a href="#fn2" class="footnote-ref" id="fnref2"
role="doc-noteref"><sup>2</sup></a>.</p>
<figure>
<img src="img/SA-STAT.CIA.png"
alt="BBS stats menu, Sole Assassin, 1994, capture d’écran d’une page d’un BBS composé de caractères ANSI" />
<figcaption aria-hidden="true">BBS stats menu, Sole Assassin, 1994,
capture d’écran d’une page d’un BBS composé de caractères
ANSI</figcaption>
</figure>
<p>Du fait qu’il est très simple de le reproduire et le modifier
(copier-coller), L’ASCII est le moyen de prédilection des memes sur les
premiers réseaux de forums, notamment visible dans l’immense archive de
<a
href="https://web.archive.org/web/20230208045620/http://www.asciiartfarts.com/">www.asciiartfarts.com
(archivé)</a> (qui contient malheureusement de très nombreux exemples
homophobes, misogynes et/ou racistes…)</p>
<figure>
<img src="img/mements.png" alt="MEMENTS, 2006, ASCII art" />
<figcaption aria-hidden="true">MEMENTS, 2006, ASCII art</figcaption>
</figure>
<p>Certains des personnages populaires d’internet viennent
spécifiquement de l’ASCII art. C’est le cas par exemple de «Kuma» (plus
tard Pedobear) ou de «Domo» dont la forme et les postures particulières
tirent leur origine de SHIFT-JIS partagées sur 2chan.</p>
<figure>
<img src="img/kuma.gif"
alt="The original «Kuma» of 2chan (trouvé sur www.knowyourmeme.com) et son équivalent contemporain" />
<figcaption aria-hidden="true">The original «Kuma» of 2chan (trouvé sur
<a
href="https://knowyourmeme.com/memes/pedobear">www.knowyourmeme.com</a>)
et son équivalent contemporain</figcaption>
</figure>
<figure>
<img src="img/2ch_AA_Characters.gif"
alt="Compilation of 2chan characters (trouvé sur www.outsiderjapan.pbworks.com) et une peluche du personnage «Domo»" />
<figcaption aria-hidden="true">Compilation of 2chan characters (trouvé
sur <a
href="http://outsiderjapan.pbworks.com/w/page/9758331/2channel?mode=print">www.outsiderjapan.pbworks.com)</a>
et une peluche du personnage «Domo»</figcaption>
</figure>
<h2 id="une-histoire-des-arts-en-mode-texte">Une histoire des arts en
mode texte</h2>
<p>À la fin des années 2000, l’Unicode devient le standard international
pour le codage informatique des caractères. Comme son nom l’indique
l’Unicode a pour but de contenir l’ensemble des types d’encodage de
caractères, rendant de ce fait obsolète les spécificités techniques
liées à l’ASCII, l’ANSI, l’ATASCII, le PETSCII, le SHIFT-JIS etc. Bien
que l’émulation d’anciens systèmes et l’application stricte de leurs
standards fait partie intégrante des pratiques des scènes ASCII
contemporaines, il est clair pour de nombreux artistes que l’ASCII art
est une notion à étendre au delà des spécificités techniques d’une
machine ou d’une autre. Certains préférent utiliser la notion de
«Textmode Art» soit «art en mode texte» comme le suggère le nom du
groupe «Textmode Friends».</p>
<p>Ce positionnement vient une nouvelle fois confirmer l’approche de
Stark de l’ASCII art comme une pratique qui ne repose pas uniquement sur
l’usage d’un système d’encodage ou d’un autre mais sur la possibilité de
créer avec les formes issues de la mécanisation du texte.</p>
<p>Ainsi, au delà de l’informatique, partout et de tout temps où le
texte mécanisé propose des contraintes, on trouve une forme spécifique
d’art en mode texte, d’ASCII art.</p>
<p>Dans <em>Neither Good, Fast, Nor Cheap: Challenges of Early Arabic
Letterpress Printing</em>, Hala Auji décrit comment les imprimeurs des
premières presses du Moyen-Orient contournent les limites de la
composition au plomb pour produire des ornements.</p>
<blockquote>
<p>Manuscripts, for example, used illumination devices, akin to
frontispieces and headpieces, called a sarlawh or ’unwan. These were
often elaborately hand-colored and gilded, to indicate the start of each
book and its subsequent chapters […]. To recall these elaborate designs
in their printed books, employees at this press creatively employed
varied ornamental sorts, as well as punctuation marks, to create similar
compositions.<a href="#fn3" class="footnote-ref" id="fnref3"
role="doc-noteref"><sup>3</sup></a></p>
</blockquote>
<blockquote>
<p>Les manuscrits, par exemple, comportaient des enluminures, à la
manière de frontispices, appelés sarlawh ou ’unwan. Ces derniers,
souvent très élaborés, étaient colorés et dorés à la main, afin
d’indiquer le début de chaque livre et des chapitres suivants […]. Pour
rappeler ces motifs dans leurs livres imprimés, les employés de cette
presse ont utilisé divers types d’ornements, ainsi que des signes de
ponctuation, reproduisant de manière créative des compositions
similaires.</p>
</blockquote>
<figure>
<img src="img/FIG3_NasifYaziji_Fasl_al_khitab_Widener.jpg"
alt="Page de Nasif al-Yaziji, Kitab Fasl al-Khitab fi Usul Lughat al-I‘rab, Beirut: American Mission Press, 1836" />
<figcaption aria-hidden="true">Page de Nasif al-Yaziji, Kitab Fasl
al-Khitab fi Usul Lughat al-I‘rab, Beirut: American Mission Press,
1836</figcaption>
</figure>
<p>Cette manière de détourner l’art de la composition typographique pour
produire des images n’est pas rare dans le domaine de l’impression au
plomb, une nécessité quand les pièces viennent à manquer, un loisir pour
les employés les plus passionés.</p>
<figure>
<img src="img/printersgrammarw0000smit_0153.jpg"
alt="printersgrammarw0000smit_0153.jpg" />
<figcaption
aria-hidden="true">printersgrammarw0000smit_0153.jpg</figcaption>
</figure>
<blockquote>
<p>Such are the shifts which sometimes are made, where neither Cuts nor
Flowers are provided, to dress the first page of a Work : and therefore
a double rule is often used ; the rather, because it takes off the
trouble of making up Head-pieces without proper Sorts.</p>
</blockquote>
<blockquote>
<p>Voici les adaptations qui sont parfois faites, quand ni séparateur ni
fleurs ne sont fournies, pour habiller la première page d’un ouvrage.
Dans ce cas une règle double est souvent utilisée ; d’autant plus
qu’elle nous évite d’improviser des frontispices sans les pièces
adéquates</p>
</blockquote>
<figure>
<img src="img/alfred_p_fluhr_improvisation.png"
alt="Improvisation, Alfred P. Fluhr, fin XVIII" />
<figcaption aria-hidden="true">Improvisation, Alfred P. Fluhr, fin
XVIII</figcaption>
</figure>
<blockquote>
<p>An improvised illustration created by Alfred P. Fluhr, an apprentice
with the Martin B. Brown Compagny, New York city, is reproduced. The
design was constructed with parenthese and rules in a playful mood
during spare moments. A little experimenting of this kind during odd
moments may help constructive ability, but the fad should not be
permitted to develop into a habit. Practical composition will be of more
benefit to a boy who aims to attain distinction as a job-printer.</p>
</blockquote>
<blockquote>
<p>Une illustration improvisée crée par Alfred P. Fluhr, un apprenti de
la Martin B. Brown Compagny, New York city, est reproduite. Le dessin
fut élaboré à l’aide de parenthèes et de règles dans une humeur espiègle
durant les moments de pauses. De temps à autre, ce genre
d’expérimentation peut aider à développer l’habilité de composition,
mais cette tendance ne doit pas devenir une habitude. La composition
concrète sera plus bénéfique à un garçon qui souhaite obtenir la
qualification d’imprimeur.</p>
</blockquote>
<p>Certains imprimeurs, comme Albert Schiller, qui choisissent
spécifiquement d’exploiter ce type de méthodes pour produire des œvres
furent en quelque sorte les ASCII artistes de leur temps.</p>
<figure>
<img src="img/TheAntiqueshop_AlbertSchiller.jpg"
alt="The Antique Shop, Albert Schiller, 1938" />
<figcaption aria-hidden="true">The Antique Shop, Albert Schiller,
1938</figcaption>
</figure>
<h2 id="lascii-art-est-il-une-relique-du-passé">L’ASCII art est-il une
relique du passé ?</h2>
<p>De la même manière que la généralisation des interfaces graphiques et
l’arrivée de l’Unicode auraient pu envoyer aux oubliettes l’ASCII art.
L’ordinateur personnel aurait pu faire disparaître le dessin à la
machine à écrire. Pourtant la pratique de cette dernière ne cesse d’être
revisitée. Ci-après un extrait de «Bob Neill’s book of typewriter» dans
lequel des images tapées à la machine à écrire sont accompagnées du
protocole permettant de les reproduire, à la main ou à l’aide d’une
machine. Ce livre, publié quelques années à peine avant le déclin rapide
et la quasi-disparition de l’usage des machines à écrire, supplantées
par le clavier d’ordinateur et les logiciels de traitement de texte,
viens en quelque sorte confirmer la survivance du médium machine à
écrire avant même la réelle mise en obsolescence de cette dernière.</p>
<figure>
<img src="img/kojak_bob_neil.jpg" alt="Kojak, Bob Neill, 1982" />
<figcaption aria-hidden="true">Kojak, Bob Neill, 1982</figcaption>
</figure>
<p><a
href="https://archive.org/details/bob-neills-book-of-typewriter-art">lien</a></p>
<p>La popularité de l’artiste James Cook est un bon exemple contemporain
de cette survivance. Ce dernier, pour le coup loin de la pratique de
l’informatique, nous propose des œvres à la machine à écrire faites sur
le motif, directement réalisée devant le modèle, même en extérieur, à la
manière d’un peintre traditionnel.</p>
<figure>
<img src="img/james_cook.jpg" alt="James Cook, outdoor render, 2020" />
<figcaption aria-hidden="true">James Cook, outdoor render,
2020</figcaption>
</figure>
<p><a
href="https://mymodernmet.com/james-cook-typewriter-art/">lien</a></p>
<p>Au travers de la longue histoire de la mécanisation du texte, malgré
l’obsolescence qui découle des processus d’innovations, les arts ASCII
ont permis à de nombreuses machines oubliées, jugées inutiles, de
ressurgir. Ils ont ainsi permis de révéler des spécificitées formelles
et culturelles impossibles à remplacer. Une manière, en quelque sorte de
prouver qu’un moyen technique ne peut jamais vraiment être réduit à
l’impertinence ou la nostalgie.</p>
<blockquote>
<p>The lure of ASCII art might not be in the nostalgia of how it looks,
but what it represents: the ideals of «cyberspace». It stands for a
wistful longing for those pre-internet days when corporations hadn’t yet
taken control of our digital day-to-day and the community was still in
control of organising itself.</p>
</blockquote>
<blockquote>
<p>L’attrait de l’ASCII art ne réside peut-être pas dans son apparente
nostalgie, mais plutôt dans ce qu’il représente : les idéaux du
«cyber-espace». Il transmet la mémoire de nos réseaux avant l’avènement
d’internet, lorsque les industriels n’avaient pas encore pris le
contrôle de notre vie quotidienne en ligne, et que les communautées
avaient encore le pouvoir de s’auto-organiser.<a href="#fn4"
class="footnote-ref" id="fnref4" role="doc-noteref"><sup>4</sup></a></p>
</blockquote>
<h2 id="jgs-font">Jgs Font</h2>
<p>Jgs Font est une famille de fontes faite en hommage à Joan G. Stark
(aka jgs, Spunk), pionnère de l’art ASCII.</p>
<p>Cette fonte a été spécialement conçue pour dessiner de l’ASCII art.
Son aspect bitmap et ses formes accentuent l’ambiguïté entre texte et
dessin. Les propriétés «graphiques» des caractères ont été exagérées en
fonction de la manière dont les ASCII artistes les utilisent.</p>
<p>Les glyphs qui composent Jgs Font peuvent se combiner d’un caractère
sur l’autre, d’une ligne sur l’autre. Elle permet, par association de
caractères, de produire des lignes continues, des courbes, des trames,
des motifs, des niveaux de gris.</p>
<p>Dans un souci de pouvoir changer de taille de corps tout en
conservant ces effets de continuitée au pixel près, la famille est
declinée en trois fontes.</p>
<p>Jgs5 pour les corps multiples de 10 : 10px, 20px, 30px etc.<br />
Jgs7 pour les corps multiples de 14 : 14px, 28px, 42px etc.<br />
Jgs9 pour les corps multiples de 18 : 18px, 36px, 54px etc.</p>
<p>Pour un meilleur résultat, la taille de corps et la hauteur de ligne
doivent être identiques et correspondre aux multiples précédement
cités.</p>
<figure>
<img src="img/vincent_bedroom.png"
alt="La chambre de Vincent, 2021, avec jgs7" />
<figcaption aria-hidden="true">La chambre de Vincent, 2021, avec
jgs7</figcaption>
</figure>
<p><a href="lien%20du%20specimen">Voir le specimen</a></p>
<p><a href="lien%20de%20la%20fonte">Télécharger la fonte</a></p>
<h2 id="galerie">Galerie</h2>
<figure>
<img src="img/DWIMMER-CAGE.PNG" alt="Cage, dwimmer, 2021 - PETSCII" />
<figcaption aria-hidden="true">Cage, dwimmer, 2021 -
PETSCII</figcaption>
</figure>
<figure>
<img src="img/LDA-SNOW.ANS.png" alt="Snow, LDA, 2021 - ANSI" />
<figcaption aria-hidden="true">Snow, LDA, 2021 - ANSI</figcaption>
</figure>
<figure>
<img src="img/croweye_by_specter.png"
alt="Croweye, Specter, 2021 - ATASCII" />
<figcaption aria-hidden="true">Croweye, Specter, 2021 -
ATASCII</figcaption>
</figure>
<figure>
<img src="img/rich_beatle.jpg"
alt="妖怪がしゃどくろ, 機動戦艦艦長, 2022 - SHIFT-JIS" />
<figcaption aria-hidden="true">妖怪がしゃどくろ, 機動戦艦艦長, 2022 -
SHIFT-JIS</figcaption>
</figure>
<figure>
<img src="img/simply_sarah.png"
alt="The Giant’s Causeway, SimplySarah, 2021 - TELETEXT" />
<figcaption aria-hidden="true">The Giant’s Causeway, SimplySarah, 2021 -
TELETEXT</figcaption>
</figure>
<figure>
<img src="img/allen_mullen.png"
alt="From “Alice in Wonderland” collection, Allen Mullen, 1999? (exact date unknown) - Newschool ASCII" />
<figcaption aria-hidden="true">From “Alice in Wonderland” collection,
Allen Mullen, 1999? (exact date unknown) - Newschool ASCII</figcaption>
</figure>
<figure>
<img src="img/jgs_dragon.png"
alt="Dragon, Joan G Stark, 1996? (exact date unknown) - Oldschool ASCII" />
<figcaption aria-hidden="true">Dragon, Joan G Stark, 1996? (exact date
unknown) - Oldschool ASCII</figcaption>
</figure>
<h2 id="autres-textes-sur-ce-quest-lascii-art">Autres textes sur ce
qu’est l’ASCII art</h2>
<ul>
<li>Polyducks</li>
</ul>
<p>ASCII art FAQs</p>
<ul>
<li>aaresour.txt</li>
<li>faq_allison.txt</li>
<li>faq_barger.txt</li>
</ul>
<p>ASCII art Tuts</p>
<ul>
<li>tut_au.txt</li>
<li>tut_crawford.txt</li>
<li>tut_jro.txt</li>
<li>tut_maija.txt</li>
<li>tut_oviatt.txt</li>
<li>tut_targon.txt</li>
<li>tut_veilleux.txt</li>
</ul>
<!-- 

Joan G. Stark
-------------

![Un autoportrait de Joan G. Stark accompagné de sa signature standard](img/jgs.png)


When I started to take interest in ASCII art I quickly came across Joan G Stark
work, feeling that I was looking at something I already knew. Whithout being
sure, it felt like looking again at drawings I saw during my childhood,
recalling the first times I experienced the internet. What is certain is that
Stark was an impressively prolific artist during the 90s and 2000s, leaving
behind a strong legacy on internet aesthetics and vernacular practices. Her
style is straight-forward, like a text mode _clear line_. She most likely drew
every possible animal, plant and monter. For each gesture and scene of everyday
life you can find one of her drawings. The same thing can be said for each
element that constitutes a landscape. 

Malheuresement Jgs à arrêter de publier depuis le début des années 2000. Son
succès et cette diversité dans son travail l'ayant exposé à de très nombreux
détournement de ses œvres sans sa permission ni attribution ou compensation,
ce que Joan à finit par ne plus supporter. As ldb sums it in « I Like Making
ASCII Art » :

> Joan was the most prolific and later, the most broken hearted as more and
> more of her ASCII art was stolen – credit for the work ripped off or claimed
> by someone else. [^4]

While today I didn't know personnaly Joan G. Stark but this name is still
resonnating in ascii art «themed» channels on discord, reddit, social medias
RESPECT ASCII ARTIST CAMPAIGN
https://asciiartist.com/from-the-original-respect-ascii-artists-campaign-page/

Jgs Font design
---------------

pour encore plus brouiller cette distinction texte-image.

supprimer, effacer la discontinuité entre les charactères, faire oublier que c'est des caractères

En faisant cela je propose une sorte de style d'ascii particulier.

Concerning specifically Jgs Font approach to ASCII art. Though it includes
CP437 characters set (ANSI art) it was designed while studying Usenet ASCII art
line-style (or oldschool), a style specific to the Usenet ASCII artists at work
between the 80s and 2000s among which Joan G Stark figures as emblematic.

While being a very strong scene at the time, leaving an immense impact on
internet aesthetic, there are few Usenet artists still active today and many
sites and links relative to the scene are now dead.

Trying to give you an idea of what this scene production look like, I've
gathered here the links and text files that I use as references to work on Jgs
Font :

-->
<section id="footnotes" class="footnotes footnotes-end-of-document"
role="doc-endnotes">
<hr />
<ol>
<li id="fn1"><p>test<a href="#fnref1" class="footnote-back"
role="doc-backlink">↩︎</a></p></li>
<li id="fn2"><p>test2<a href="#fnref2" class="footnote-back"
role="doc-backlink">↩︎</a></p></li>
<li id="fn3"><p>test3<a href="#fnref3" class="footnote-back"
role="doc-backlink">↩︎</a></p></li>
<li id="fn4"><p>test2<a href="#fnref4" class="footnote-back"
role="doc-backlink">↩︎</a></p></li>
</ol>
</section>
</body>
</html>
